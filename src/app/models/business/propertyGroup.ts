export class PropertyGroup {
  name: string;
  details: string;
  isEnabled: boolean;
  hasDetails: boolean;
}
