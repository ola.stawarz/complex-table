import { PropertyGroup } from "./propertyGroup";

export class PropertyDetails {
  id: number;
  name: string;
  group1: PropertyGroup;
  group2: PropertyGroup;
  group3: PropertyGroup;
}
