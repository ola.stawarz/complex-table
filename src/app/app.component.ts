import { Component } from '@angular/core';
import { DATA } from './data';
import { DataModel } from './DataModel';
import { Property } from './models/business/property';
import { PropertyDetails } from './models/business/propertyDetails';
import { PropertyService } from './services/business/property.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  value: number = 50;
  properties: Property[];
  propertyDetails: PropertyDetails;

  constructor(private propertyService: PropertyService) {}

  ngOnInit(): void {
    this.propertyService.getProperties().subscribe((properties) => {
      this.properties = properties;
    });
  }
    
  toggle(event: Event, idx: number): void {
    
  }
}
