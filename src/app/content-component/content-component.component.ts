import { Component, OnInit } from '@angular/core';
import { PropertyService } from 'src/app/services/business/property.service';
import { PropertyDetails } from 'src/app/models/business/propertyDetails';

@Component({
  selector: 'app-content-component',
  templateUrl: './content-component.component.html',
  styleUrls: ['./content-component.component.scss']
})
export class ContentComponentComponent implements OnInit {

  propertyDetails: PropertyDetails;

  constructor(
      private propertyService: PropertyService,
  ) { }

  ngOnInit(): void {
    this.propertyService
      .getPropertyDetails(1)
      .subscribe((response) => {
        this.propertyDetails = response;
        });
  }

}
