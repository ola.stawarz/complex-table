import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { PropertyDetails } from 'src/app/models/business/propertyDetails';
import { Property } from 'src/app/models/business/property';


@Injectable({
  providedIn: 'root',
})
export class PropertyService {

  constructor() {}

  getProperties(): Observable<Property[]> {
    return of([
      {
          "id": 1,
          "street": "Hainer Weg 15",
          "date": "2023-01-01"
      },
      {
          "id": 2,
          "street": "Hainer Weg 16",
          "date": "2022-01-01"
      }
    ]);
  }

  getPropertyDetails(bid: number): Observable<PropertyDetails> {
    console.log('Details for property with id are loaded: ' + bid);
    return of({
        id: bid,
        name: 'Status Workorder',
        group1:
        {
          name: 'Exploration',
          details: 'Exploration',
          isEnabled: true,
          hasDetails: false,
        },
        group2:
        {
          name: 'NE3',
          details: 'NE3',
          isEnabled: false,
          hasDetails: false,
        },
        group3:
        {
          name: 'NE4',
          details: 'NE4',
          isEnabled: true,
          hasDetails: true,
        }
      });
   }
}
