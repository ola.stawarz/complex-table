import { DataModel } from "./DataModel";

export const DATA: DataModel[] = [
    {
        "id": 1,
        "street": "Hainer Weg 15",
        "date": "2023-01-01"
    },
    {
        "id": 2,
        "street": "Hainer Weg 16",
        "date": "2022-01-01"
    }
]