import { Component, Input, OnInit } from '@angular/core';
import { DataModel } from '../DataModel';
import { Property } from '../models/business/property';

@Component({
  selector: 'app-header-component',
  templateUrl: './header-component.component.html',
  styleUrls: ['./header-component.component.scss']
})
export class HeaderComponentComponent implements OnInit {

  @Input()
  property: Property;

  value: number = 50;
  
  constructor() { }

  ngOnInit(): void {
  }

}
